package main

import (
	// "errors"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"time"
)

// set up the control fifo
// absolute path of the fifo used to send commands to the show process
var fifoPath string = getFifoPath()

var pipedreamDir = ""

// globals

var templates = template.Must(loadTemplates("templates"))

// array of strings containing the names of shows
// XXX should be queried from the python show process
var allShowNames = []string{}

//
// SUBPROCESS MANAGEMENT
//

// Shows is a simple array of show names (used only for json typing)
type Shows struct {
	Shows []string `json:"shows"`
}

func getAvailableShows() []string {
	subCmd := exec.Command("/usr/bin/python2.7", "go.py", "--list")
	subCmd.Dir = pipedreamDir

	out, err := subCmd.Output()
	if err != nil {
		panic("couldn't get show list!")
	}
	ks := Shows{}
	json.Unmarshal([]byte(out), &ks)
	return ks.Shows
}

// XXX all kinds of random environmental commands may be passed?
func runShow(showName string, exitChan chan int) (cmd *exec.Cmd, err error) {

	// pwd, err := os.Getwd()
	// if err != nil {
	// 	panic("couldn't get working directory")
	// }
	// log.Printf("runShow wd:%v", pwd)

	subCmd := exec.Command("/usr/bin/python2.7", "go.py", showName)

	// just redirect worker process to our stdout/stderr
	// XXX this could go to a file that we could tail from the webui
	subCmd.Stdout = os.Stdout
	subCmd.Stderr = os.Stderr

	// set the environment to the control fifo
	newEnv := append(os.Environ(), "FIFO_CONTROL="+fifoPath)
	// fmt.Println("setting new environment:")
	// fmt.Println(newEnv)
	subCmd.Env = newEnv

	subCmd.Dir = pipedreamDir

	err = subCmd.Start()
	if err != nil {
		log.Printf("Can't start subprocess! %v", err)
		return nil, err
	}

	log.Printf("%v", subCmd.Process)
	log.Printf("pid: %v", subCmd.Process.Pid)

	// start an anon goroutine to wait for the sub to exit
	go func(cmd *exec.Cmd) {
		log.Println("waiting for subprocess")
		cmd.Wait()
		if err != nil {
			// XXX when does this even get hit?
			log.Printf("Process unexpectedly exited! %v", err)
		}

		ws := cmd.ProcessState.Sys().(syscall.WaitStatus)
		exitCode := ws.ExitStatus()
		log.Printf("exit code: %v", exitCode)

		exitChan <- exitCode
	}(subCmd)

	return subCmd, nil
}

// return absolute path of the named pipe / fifo used to communicate with child shows
// one file is used for all shows
// XXX maybe this means the fifo needs to be drained between shows?
func getFifoPath() string {
	const baseDir = "/tmp"

	// base the fifo path off the parent pid
	fifoPath := filepath.Join(baseDir, fmt.Sprintf("runshow_fifo_%d", os.Getpid()))

	stat, err := os.Stat(fifoPath)
	if stat != nil {
		// file already exists? now what?
		log.Printf("fifo already exists: %v", stat)
		panic("fifo already exists!")
	}
	if err == nil {
		panic("Misinterpreted stat results in getFifoPath")
	}
	if os.IsNotExist(err) {
		err = syscall.Mkfifo(fifoPath, 0600)
		if err != nil {
			panic("couldn't create fifo!")
		}
		// clean up the fifo when this process terminates
		log.Printf("Created fifo file at: %v", fifoPath)
		return fifoPath
	}
	panic("couldn't create fifo")
}

// open the fifo, write a command string and then close it
func writeFifoCommand(fifoPath string, cmd string) error {
	f, err := os.OpenFile(fifoPath, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	if _, err := f.Write([]byte(cmd)); err != nil {
		return err
	}
	if err := f.Close(); err != nil {
		return err
	}
	return nil
}

//
// STATE MANAGEMENT
//
var serverCommandChan = make(chan *serverCommand, 2)

type serverCommand struct {
	Command   string
	IntParam  int
	StrParam  string
	BoolParam bool
	resp      chan serverResp
}

type serverResp struct {
	State serverState
	Err   error
}

type serverState struct {
	Running bool `json:"running"`
	// brightness 0-100
	Bright      int    `json:"brightness"`
	CurrentShow string `json:"currentShow"`
}

func manageState(commandChannel chan *serverCommand) {
	log.Println("Starting state management goroutine!")

	// canonical state is private to this goroutine
	state := serverState{Running: false, Bright: 100, CurrentShow: allShowNames[0]}
	log.Println("initial state:", state)

	// subprocess management is here too
	exitChan := make(chan int, 1)
	var subproc *exec.Cmd
	var startTime *time.Time
	shouldStopShow := false

	// inner functions have to be defined as lambdas
	startShow := func(name string) error {
		cmd, err := runShow(name, exitChan)
		if err != nil {
			log.Println("couldn't start new show")
			return err
		}
		log.Printf("started show:%v", name)
		subproc = cmd
		// ug golang :/
		t := time.Now()
		startTime = &t
		return nil
	}

	stopShow := func() error {
		// XXX try shutting it down with named pipe first?
		// XXX may present an interesting race condition?
		proc := subproc.Process
		log.Printf("Killing process: %v", *proc)
		proc.Kill()
		subproc = nil
		startTime = nil
		return nil
	}

	// main work loop
	for {
		var cmd *serverCommand

		// wait for a message from the commandChannel or a running show to exit
		select {
		case cmd = <-commandChannel:
			log.Printf("got new server command:%v", cmd)

		case exitCode := <-exitChan:
			log.Printf("got exit code:%v", exitCode)
			cmd = &serverCommand{Command: "exited", IntParam: exitCode}
		}

		switch cmd.Command {
		case "exited":
			if !shouldStopShow {
				if startTime != nil {
					after := time.Now().Sub(*startTime)
					log.Printf("Show unexpectedly exited after %v, restarting!", after)
					// XXX do something smarter here if go.py is exiting immediately
				} else {
					log.Printf("Show unexpectedly exited, restarting!")
				}
				startShow(state.CurrentShow)
			}

		case "status":
			// don't actually need to do anything
		case "running":
			if state.Running != cmd.BoolParam {
				// XXX actually need to handle changing running state
				state.Running = cmd.BoolParam
				log.Printf("Changing running state to:%v", state.Running)

				if state.Running {
					shouldStopShow = false
					startShow(state.CurrentShow)
				} else {
					shouldStopShow = true
					stopShow()
				}
			}
		case "bright":
			if state.Bright != cmd.IntParam {
				state.Bright = cmd.IntParam
				log.Printf("Changed brightness to:%v", state.Bright)
				// XXX invoke fifo command
			}
		case "show":
			if state.CurrentShow != cmd.StrParam {
				state.CurrentShow = cmd.StrParam
				if subproc != nil {
					stopShow() // XXX errors
				}
				log.Printf("Changed show to:%v", state.CurrentShow)

			}
		case "color":
			log.Printf("XXX should change color to:%v", cmd.StrParam)
			// XXX this should probably be a showParam struct?
			// XXX invoke fifo command
		default:
			log.Printf("ignoring command:%v", cmd.Command)
		}

		// command may want a reply
		if cmd.resp != nil {
			// XXX not sure if this resp can be omitted and just return state
			resp := serverResp{State: state}
			cmd.resp <- resp
		}
	}
}

//
// view handlers
//

func indexHandler(w http.ResponseWriter, r *http.Request) {

	// XXX does golang have anonymous types? or should i just go with this?
	type indexData struct {
		Title   string
		Message string
	}

	p := indexData{Title: "it's time to light the lights", Message: "Gunter is sleeping"}

	renderTemplate(w, "index", p)
}

// general http get/post handler for querying & changing state
func statusHandler(w http.ResponseWriter, r *http.Request) {

	var resp serverResp

	switch r.Method {
	case http.MethodGet:
		// get state from the state goroutine by sending a status message
		cmd := &serverCommand{
			Command: "status",
			resp:    make(chan serverResp)}
		serverCommandChan <- cmd

		// then waiting on the message's response channel
		resp = <-cmd.resp

	case http.MethodPost:
		// client posts back a partial serverState struct
		// so try to decode into that
		// XXX except we can't detect unset values
		type clientState struct {
			Running     *bool   `json:"running,omitempty"`
			Bright      *int    `json:"brightness,omitempty"`
			CurrentShow *string `json:"show,omitempty"`
			Color       *string `json:"color,omitempty"`
		}

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		var msg clientState
		err = json.Unmarshal(body, &msg)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		// this posts one message to the goroutine for each
		// parameter to be changed - browser only tries to
		// change one at a time right now so this should be ok?
		var cmd *serverCommand

		if msg.Running != nil {
			// log.Printf("set running:%v\n", *msg.Running)
			cmd = &serverCommand{
				Command:   "running",
				BoolParam: *msg.Running,
				resp:      make(chan serverResp)}
			serverCommandChan <- cmd
		}
		if msg.Bright != nil {
			// log.Printf("set brightness:%v\n", *msg.Bright)
			cmd = &serverCommand{
				Command:  "bright",
				IntParam: *msg.Bright,
				resp:     make(chan serverResp)}
			serverCommandChan <- cmd
		}
		if msg.CurrentShow != nil {
			// log.Printf("set show:%v\n", *msg.CurrentShow)
			cmd = &serverCommand{
				Command:  "show",
				StrParam: *msg.CurrentShow,
				resp:     make(chan serverResp)}
			serverCommandChan <- cmd
		}
		if msg.Color != nil {
			// log.Printf("set color:%v\n", *msg.Color)
			cmd = &serverCommand{
				Command:  "color",
				StrParam: *msg.Color,
				resp:     make(chan serverResp)}
			serverCommandChan <- cmd
		}

		resp = <-cmd.resp

	default:
		http.Error(w, "Invalid method", http.StatusBadRequest)
		return
	}

	// log.Printf("Sending state:%v", resp)

	if resp.Err != nil {
		log.Println("!!! Error in state response")
		http.Error(w, "mysterious problem", http.StatusInternalServerError)
		return
	}
	returnedState := resp.State

	if err := json.NewEncoder(w).Encode(returnedState); err != nil {
		panic(err) // XXX probably don't really want to panic here
	}
}

// http get/post handler to query available shows & switch between them
func showHandler(w http.ResponseWriter, r *http.Request) {

	type showsResponse struct {
		Active string   `json:"activeShow"`
		Shows  []string `json:"showNames"`
	}

	if r.Method != http.MethodGet {
		http.Error(w, "Invalid method", http.StatusBadRequest)
		return
	}

	resp := showsResponse{Active: allShowNames[0], Shows: allShowNames}

	if err := json.NewEncoder(w).Encode(resp); err != nil {
		panic(err) // XXX probably don't really want to panic here
	}
}

//
// main and all that
//
func cleanup() {
	if fifoPath != "" {
		log.Printf("cleaning up fifo: %v", fifoPath)
		os.Remove(fifoPath)
		fifoPath = ""
	}
}

func main() {
	// make sure we know where to find pipedream
	val, ok := os.LookupEnv("PIPEDREAM_DIR")
	if !ok {
		panic("Must set PIPEDREAM_DIR environment variable")
	}
	pipedreamDir = val
	// and make sure the value looks reasonable
	stat, err := os.Stat(pipedreamDir)
	if err != nil {
		panic("can't stat PIPEDREAM_DIR")
	}
	if !stat.IsDir() {
		panic("PIPEDREAM_DIR is not set to a directory")
	} // ... ok good enough
	log.Printf("Using pipedream install at:%v\n", pipedreamDir)

	// get list of available shows
	allShowNames = getAvailableShows()
	log.Printf("Loaded shows: %v\n", allShowNames)

	// setup cleanup handler to remove fifo
	c := make(chan os.Signal, 2)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		cleanup()
		os.Exit(1)
	}()
	// won't be run from a normal exit
	defer cleanup()

	// configure http server
	http.HandleFunc("/", indexHandler)

	http.HandleFunc("/data/status", statusHandler)
	http.HandleFunc("/data/shows", showHandler)

	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))

	// start worker goroutines before blocking on web server
	go manageState(serverCommandChan)

	port := ":8080" // listens on all interfaces
	log.Printf("Listening on port %s\n", port)
	log.Fatal(http.ListenAndServe(port, nil))
}

//
// template helpers
//
func loadTemplates(directory string) (*template.Template, error) {

	files, err := ioutil.ReadDir(directory)
	if err != nil {
		log.Fatal(err)
	}

	templateFiles := make([]string, 0)

	for _, file := range files {
		if !file.IsDir() && strings.HasSuffix(file.Name(), ".html") {
			templateFiles = append(templateFiles, filepath.Join(directory, file.Name()))
		}
	}
	log.Println("Loaded templates", templateFiles)

	return template.ParseFiles(templateFiles...)
}

func renderTemplate(w http.ResponseWriter, tmpl string, dat interface{}) {
	err := templates.ExecuteTemplate(w, tmpl+".html", dat)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
