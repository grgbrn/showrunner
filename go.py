#!/usr/bin/python2.7
"""
Simpler process that mocks the behavior of a pipedream show
"""

from __future__ import division, print_function # inching towards py3k

import os
import sys
import time

def getmillis():
    return time.time() * 1000


def main(show_name):
    # if 'FIFO_CONTROL' is set in the environment then we need
    # to monitor a named pipe for commands
    # very important that the fifo is opened nonblocking!
    control = None
    fifo_name = os.getenv('FIFO_CONTROL')
    if not fifo_name:
        print("FIFO_CONTROL env var not set, no external controls")
    else:
        if not os.path.exists(fifo_name):
            print("Can't find control FIFO {}, running anyway".format(fifo_name))
        else:
            print("Monitoring FIFO {} for commands".format(fifo_name))
            control = os.open(fifo_name, os.O_RDONLY | os.O_NONBLOCK);

    print("starting fake show")
    is_running = True

    bright = 50

    while True:
        print("show={} bright={} running={}".format(show_name, bright, is_running))
        time.sleep(1)

        if control:
            try:
                _cmd = os.read(control, 80)
                if _cmd:
                    _cmd = _cmd.strip() # XXX encoding?
                    if _cmd == "pause":
                        print("pausing show")
                        is_running = False
                    elif _cmd == "start":
                        print("restarting show")
                        is_running = True
                    elif _cmd.startswith("set_bright="):
                        v = int(_cmd[11:])
                        print("setting brightness to {}".format(v))
                        bright = v
                    else:
                        print("ignoring unknown command:{}".format(_cmd))

            except Exception as e:
                print("Exception trying to read named pipe:{}".format(e))


if __name__ == '__main__':
    import argparse

    # load shows
    # show_map = dict(shows.load_shows())

    parser = argparse.ArgumentParser(description='fakeshow')

    parser.add_argument('-v', '--verbose', action='store_true')

    parser.add_argument('show', metavar='show_name', type=str,
                        help='name of show to run')

    args = parser.parse_args()

    # if args.verbose:
    #     print("loaded {} shows:".format(len(show_map)))
    #     print(show_map)
    #     print()

    # if len(args.shows) == 0:
    #     print('Available shows:')
    #     print(', '.join(show_map.keys()))
    #     print()
    #     parser.print_usage()
    #     sys.exit(0)

    main(args.show)
