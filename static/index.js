// see how this works, define constant dom refs at the top
var gunter = document.getElementById("gunter");

var brightInput = document.getElementById("brightInput");
var brightVal = document.getElementById("brightVal")

var colorInput = document.getElementById("colorPicker")


var isRunning = false;

function setBrightnessLabel(num) {
    var brightVal = document.getElementById("brightVal")
    brightVal.innerText = num;
}

function setRunningState(r) {
    isRunning = r
    if (isRunning) {
        gunter.classList.remove("greyed-out")
    } else {
        gunter.classList.add("greyed-out")
    }
}

function changeShow(showName) {
    const payload = JSON.stringify({show:showName});
    console.log(">>> posting:" + payload)

        fetch("/data/status", {
            body: payload,
            method: "POST"
        })
        .then(resp => resp.json())
        .then(handleStatusResponse)
}

function handleStatusResponse(val) {
    console.log("handling status response:")
    console.log(val)

    setRunningState(val.running);

    setBrightnessLabel(val.brightness);
    // also need to set the initial position of the slider
    brightInput.value = val.brightness

    var sel = document.getElementById("showList")
    // console.log(sel)
    // console.log(sel.value)
    sel.value = val.currentShow

}


document.addEventListener('DOMContentLoaded', function() {
    //////////////////////////
    // setup event listeners
    //////////////////////////
    console.log("ok loaded index js and all that");

    gunter.addEventListener("click", function(e) {
        console.log("gunter is ticklish!");

        // generate a payload to toggle the state of running
        const payload = JSON.stringify({running:!isRunning});
        console.log("posting:")
        console.log(payload)

        fetch("/data/status", {
            body: payload,
            method: "POST"
        })
        .then(resp => resp.json())
        .then(handleStatusResponse)
    })

    function handleRangeInput(e) {
        const newBrightness = e.target.valueAsNumber;
        console.log("change: setting brightness=" + newBrightness);

        const payload = JSON.stringify({brightness:newBrightness});
        console.log(">>> posting:" + payload)

        fetch("/data/status", {
            body: payload,
            method: "POST"
        })
        .then(resp => resp.json())
        .then(handleStatusResponse)
    }

    // fired continuously to allow for scrubbing
    // XXX do something to rate-limit this on the js side?
    brightInput.addEventListener("input", handleRangeInput)

    // fired once on control release
    // always fire this one so the final value won't be throttled
    // XXX this seems unreliable on iOS
    brightInput.addEventListener("change", handleRangeInput)

    // color input
    function handleColorInput(e) {
        const newColor = e.target.value;
        console.log("changed color:" + newColor);

        const payload = JSON.stringify({color:newColor});
        console.log(">>> posting:" + payload)

        fetch("/data/status", {
            body: payload,
            method: "POST"
        })
        .then(resp => resp.json())
        .then(handleStatusResponse)
    }
    // fired every time color changes (should be throttled)
    colorInput.addEventListener("input", handleColorInput)
    // fired on final selection (don't throttle!)
    colorInput.addEventListener("change", handleColorInput)

    ///////////////////
    // initialize app
    ///////////////////
    console.log("trying to initialize with server state")

    // initial GET to load list of available shows
    fetch('/data/shows')
    .then(resp => resp.json())
    .then(val => {
        // XXX don't let this response set the active show for now
        console.log("response from /data/shows")
        console.log(val)
        var sel = document.getElementById("showList")
        var elts = val.showNames.map(name => {
            // const isSelected = (name == val.activeShow) ? "selected " : "";
            const isSelected = ""
            return `<option ${isSelected} value=${name}>${name}</option>`
        });

        var markup = elts.join('');
        sel.innerHTML = markup;

        sel.addEventListener('change', e => {
            var newShow = e.target.value;
            changeShow(e.target.value);
        })
    })

    // then a second GET to status endpoint to load initial state
    // XXX slight race condition this should wait until the
    // first one has finished for sure
    fetch('/data/status')
    .then(resp => resp.json())
    .then(handleStatusResponse)    

});


